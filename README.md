This repository holds the firmware update files for the Kino Flo Select and Diva LED products.

Please download the zip file from this repository:

After you unzip on a Windows PC please install the Renesas_Flash_Programmer_Package_V20502.exe by double clicking.

Once installed you can go into the Flash6800232-PROD folder and double click on the Flash6800232-PROD.rws file.

For BETA COLOR software: Click the browse button to select the programming file "Flash6800232-BETA_COLOR-VX_XX.srec" and press the Start button.
